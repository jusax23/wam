import wam from "./db.js";


const db_struct = {
  accounts: {
      ID:                 {type:"int",      A_I:true},
      accountId:          {type:"text"              },
      name:               {type:"text"              },
      accountkey:         {type:"text"              },
  },
};

const host="localhost", port=3306, user="user", password="12345678", database="testDB";

const db = new wam({host, port, user, password, database})

db.connect()
.then(()=>db.check(db_struct))
.then(()=>console.log("db is now ready!"))
.catch(err=>{
  console.log("Can not connect properly to the Database: ",err);
  process.exit();
});
